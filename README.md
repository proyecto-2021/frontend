# AGATHA 0.0.1

# Repositorio Web. Sistemas de información 2021

Entorno mobile. Proyecto de sistemas de información. Proyecto que se realizará por los estudiantes del décimo semestre de ingeniería Informática de la UCLA.

## Integrantes de Equipo

- Rhonald Sánchez (Líder de Equipo)
- Farialys Martinez
- Andrea Pérez
- Jesus Brito
- Marcell Vieira

### Especificaciones técnicas / Referencias

- Documentación oficial `https://vuetifyjs.com/` `https://vuejs.org/ `

#### Requerimientos previos:

- NodeJS
- Npm
- Vue CLI `https://cli.vuejs.org/`

#### Branch

- master -> Production `http://dev.najoconsultores.com:20045/`
- staging -> Pre Production `https://fervent-booth-3dc597.netlify.app/`
- develop -> Develop
-

### Configurar el entorno de desarrollo

#### Comandos para desplegar funciones.

| °   | Comando         | Descripción                                   | Notas |
| --- | --------------- | --------------------------------------------- | ----- |
| 1   | `npm run serve` | Contrucción de la aplicación para desarrollo  |
| 2   | `npm run build` | Construcción de la aplicación para producción |
| 3   | `npm run lint`  | Linters y arreglar archivos                   |

### Folder Structure

```
├──public
|   ├──favicon.ico
|   └──index.html
├──src:
|   ├──assets:
|   │   ├── ...
|   ├──components:
|   │   ├── ...
|   ├──enums:
|   │   ├── ...
|   ├──layouts:
|   │   ├── ...
|   ├──plugins:
|   │   ├── ...
|   ├──repositories:
|   │   ├── ...
|   ├──router:
|   │   ├── ...
|   ├──services:
|   │   ├── ...
|   ├──store:
|   │   ├── ...
|   ├──types:
|   │   ├── ...
|   ├──views:
|   │   ├── ...
|   ├──App.vue
|   ├──main.ts
|   ├──shims-tsx.d.ts
|   ├──shims-vue.d.ts
|   └──shims-vuetify.d.ts
├──browserslistrc
├──env.development
├──env.production
├──.eslintignore
├──.eslintrc.js
├──.gitignore
├──.prettierignore
├──.prettierrc.js
├──babel.config.js
├──package.json
├──package-lock.json
├──README.md
├──tsconfig.js
└──vue.config.js
```

### Notas

- La aplicación fue creada por medio de vue cli.
- Paquetes y dependecias utilizadas para la elaboración:

| °   | Paquete                            | Versión         |
| --- | ---------------------------------- | --------------- |
| 1   | `apexcharts`                       | `"^3.27.3`      |
| 2   | `axios`                            | `^0.21.1`       |
| 3   | `core-js`                          | `^3.6.5`        |
| 4   | `vue`                              | `^2.6.11`       |
| 5   | `vue-apexcharts`                   | `^1.6.2`        |
| 6   | `vue-router`                       | `^3.2.0`        |
| 7   | `vue-sweetalert2`                  | `^4.3.1`        |
| 8   | `vuetifye`                         | `^2.4.0`        |
| 9   | `vuex`                             | `^3.4.0`        |
| 10  | `vuex-persistedstate`              | `^4.0.0-beta.3` |
| 11  | `@typescript-eslint/eslint-plugin` | `^4.18.0`       |
| 12  | `@typescript-eslint/parser`        | `^4.18.0`       |
| 13  | `@vue/cli-plugin-babel`            | `~4.5.`         |
| 14  | `@vue/cli-plugin-eslint`           | `~4.5.0`        |
| 15  | `@vue/cli-plugin-router`           | `~4.5.0`        |
| 16  | `@vue/cli-plugin-typescript`       | `~4.5.0`        |
| 17  | `@vue/cli-plugin-vuex`             | `~4.5.0`        |
| 18  | `@vue/cli-service`                 | `~4.5.0`        |
| 19  | `@vue/eslint-config-prettier`      | `^6.0.0`        |
| 20  | `@vue/eslint-config-typescript`    | `^7.0.0`        |
| 21  | `eslint`                           | `^6.7.2`        |
| 22  | `eslint-plugin-prettier`           | `^3.3.1`        |
| 23  | `eslint-plugin-vue`                | `^6.2.2`        |
| 24  | `lint-staged`                      | `^9.5.0`        |
| 25  | `prettier`                         | `^2.2.1`        |
| 26  | `sass`                             | `^1.32.0`       |
| 27  | `sass-loader`                      | `^10.0.0`       |
| 28  | `typescript`                       | `~4.1.5`        |
| 29  | `vue-cli-plugin-vuetify`           | `~2.4.0`        |
| 30  | `vue-template-compiler`            | `^2.6.11`       |
| 31  | `vuetify-loader`                   | `^1.7.0`        |

#### Environment

- Path Develop: `https://agatha-develop.herokuapp.com/api/`
- Path Production: `https://dev.najoconsultores.com/api/`
