import { RoleRestRepository, CreateRole, UpdateRole } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class RoleRepository
  extends BaseRepository
  implements RoleRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/role';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.getResource('/all');
  }

  async getAllActives(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: string): Promise<ResponseWrapper> {
    return await this.getById(id);
  }
  async getByUsername(username: string): Promise<ResponseWrapper> {
    return await this.getResource(`/user/${username}`);
  }
  async createData(data: CreateRole): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.create(formData);
  }
  async updateData(data: UpdateRole): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.updateById(data.id, formData);
  }

  async activeData(id: number): Promise<ResponseWrapper> {
    return await this.patchById(id);
  }

  async removeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.remove(id);
  }
}
