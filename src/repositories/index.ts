import axios, { AxiosInstance } from 'axios';
import { BlogRepository as Blog } from '@/repositories/BlogRepository';
import { UserRepository as User } from '@/repositories/UserRepository';
import { GathererRepository as Gatherer } from '@/repositories/GathererRepository';
import { ReportRepository as Report } from '@/repositories/ReportRepository';
import { AdminRepository as Admin } from '@/repositories/AdminRepository';
import { MissionRepository as Mission } from '@/repositories/MissionRepository';
import { AuthRepository as Auth } from '@/repositories/AuthRepository';
import { CreatureRepository as Creature } from '@/repositories/CreatureRepository';
import { WizardRepository as Wizard } from '@/repositories/WizardRepository';
import { RoleRepository as Role } from '@/repositories/RoleRepository';
import { CreatureVersionRepository as CreatureVersion } from '@/repositories/CreatureVersionRepository';
import { TaskRepository as Task } from '@/repositories/TaskRepository';
import { ReportRejectedRepository as ReportRejected } from '@/repositories/ReportRejectedRepository';
import { MissionTaskRepository as MissionTask } from '@/repositories/MissionTaskRepository';
import { CategoryRepository as Category } from '@/repositories/CategoryRepository';
import { DashboardRepository as Dashboard } from '@/repositories/DashboardRepository';

import {
  BlogRestRepository,
  UserRestRepository,
  GathererRestRepository,
  ReportRestRepository,
  AdminRestRepository,
  MissionRestRepository,
  AuthRestRepository,
  CreatureRestRepository,
  WizardRestRepository,
  RoleRestRepository,
  CreatureVersionRestRepository,
  TaskRestRepository,
  ReportRejectedRestRepository,
  MissionTaskRestRepository,
  CategoryRestRepository,
  DashboardRestRepository,
} from '@/types';

let BlogRepository: BlogRestRepository;
let UserRepository: UserRestRepository;
let GathererRepository: GathererRestRepository;
let ReportRepository: ReportRestRepository;
let AdminRepository: AdminRestRepository;
let MissionRepository: MissionRestRepository;
let TaskRespository: TaskRestRepository;
let AuthRepository: AuthRestRepository;
let CreatureRepository: CreatureRestRepository;
let WizardRepository: WizardRestRepository;
let RoleRepository: RoleRestRepository;
let CreatureVersionRepository: CreatureVersionRestRepository;
let ReportRejectedRepository: ReportRejectedRestRepository;
let MissionTaskRepository: MissionTaskRestRepository;
let CategoryRepository: CategoryRestRepository;
let DashboardRepository: DashboardRestRepository;

function initializeRepositories($axios: AxiosInstance): void {
  BlogRepository = new Blog($axios);
  UserRepository = new User($axios);
  GathererRepository = new Gatherer(axios);
  ReportRepository = new Report(axios);
  AdminRepository = new Admin($axios);
  MissionRepository = new Mission($axios);
  AuthRepository = new Auth($axios);
  CreatureRepository = new Creature($axios);
  WizardRepository = new Wizard($axios);
  RoleRepository = new Role($axios);
  CreatureVersionRepository = new CreatureVersion($axios);
  TaskRespository = new Task($axios);
  ReportRejectedRepository = new ReportRejected($axios);
  MissionTaskRepository = new MissionTask($axios);
  CategoryRepository = new Category($axios);
  DashboardRepository = new Dashboard($axios);
}

export {
  initializeRepositories,
  BlogRepository,
  UserRepository,
  GathererRepository,
  ReportRepository,
  AdminRepository,
  MissionRepository,
  AuthRepository,
  CreatureRepository,
  WizardRepository,
  RoleRepository,
  CreatureVersionRepository,
  TaskRespository,
  ReportRejectedRepository,
  MissionTaskRepository,
  CategoryRepository,
  DashboardRepository,
};
