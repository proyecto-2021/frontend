import { DashboardRestRepository } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class DashboardRepository
  extends BaseRepository
  implements DashboardRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/dashboard';
  }
  async getData(): Promise<ResponseWrapper> {
    return await this.get();
  }
}
