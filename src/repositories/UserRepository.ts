import {
  UpdatePassword,
  UserRestRepository,
  User,
  UpdatePasswords,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class UserRepository
  extends BaseRepository
  implements UserRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/users';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }
  async getByUsername(username: string): Promise<ResponseWrapper<any>> {
    return await this.getById(username);
  }

  async createData(data: User): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async getUserProfile(): Promise<ResponseWrapper> {
    return await this.getResource('Profile');
  }

  async activeData(username: string): Promise<ResponseWrapper<any>> {
    return await this.patchById(username);
  }

  async removeData(username: string): Promise<ResponseWrapper<any>> {
    return await this.remove(username);
  }

  async updatePassword(data: UpdatePassword): Promise<ResponseWrapper> {
    return await this.patchResource('updateMyPassword', data);
  }

  async updatePasswords(data: UpdatePasswords): Promise<ResponseWrapper> {
    return await this.patchResource(`updatePassword/${data.uername}`, data);
  }
}
