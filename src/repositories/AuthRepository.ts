import {
  AuthRestRepository,
  Login,
  RestartPassword,
  Register,
  ResponseAuth,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class AuthRepository
  extends BaseRepository
  implements AuthRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/auth';
  }
  async login(data: Login): Promise<ResponseWrapper> {
    return await this.createResource('login', data);
  }
  async restartPassword(data: RestartPassword): Promise<ResponseWrapper> {
    return await this.createResource('restartPassword', data);
  }
  async register(data: Register): Promise<ResponseWrapper> {
    return await this.createResource('register', data);
  }
}
