import { GathererRestRepository } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';
import { CreateGatherer, UpdateGatherer } from '@/types';

export class GathererRepository
  extends BaseRepository
  implements GathererRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/rcollectors';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: number): Promise<ResponseWrapper<any>> {
    return await this.getById(id);
  }

  async getByUsername(username: string): Promise<ResponseWrapper<any>> {
    return await this.getById(username);
  }

  async createData(data: CreateGatherer): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.create(formData);
  }

  async updateDataById(data: UpdateGatherer): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.updateById(data.id, formData);
  }

  async removeData(id: number): Promise<ResponseWrapper> {
    return await this.remove(id);
  }
}
