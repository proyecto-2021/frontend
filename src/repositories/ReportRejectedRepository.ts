import { ReportRestRepository } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';
import { CreateReportRejected, UpdateReportRejected } from '@/types';

export class ReportRejectedRepository
  extends BaseRepository
  implements ReportRejectedRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/reports/rejected';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async createData(data: CreateReportRejected): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async updateDataById(data: UpdateReportRejected): Promise<ResponseWrapper> {
    return await this.updateById(data.id, data);
  }

  async removeData(id: string): Promise<ResponseWrapper> {
    return await this.remove(id);
  }
}
