import {
  CategoryRestRepository,
  CreateCategory,
  UpdateCategory,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class CategoryRepository
  extends BaseRepository
  implements CategoryRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/categories';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: number): Promise<ResponseWrapper> {
    return await this.getById(id);
  }

  async createData(data: CreateCategory): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async updateData(data: UpdateCategory): Promise<ResponseWrapper> {
    return await this.updateById(data.id, data);
  }

  async removeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.remove(id);
  }
}
