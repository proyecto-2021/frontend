import {
  MissionTaskRestRepository,
  CreateMissionTask,
  UpdateMissionTask,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class MissionTaskRepository
  extends BaseRepository
  implements MissionTaskRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/submissions';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async createData(data: CreateMissionTask): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async getByIdData(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/${id}`);
  }

  async updateData(data: UpdateMissionTask): Promise<ResponseWrapper> {
    return await this.updateById(data.id, data);
  }

  async getByMissionId(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/mission/${id}`);
  }

  async removeData(id: number): Promise<ResponseWrapper> {
    return await this.remove(id);
  }
}
