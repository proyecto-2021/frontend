import {
  CreateCreatureVersion,
  CreatureVersionRestRepository,
  UpdateCreatureVersion,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class CreatureVersionRepository
  extends BaseRepository
  implements CreatureVersionRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/creatures/versions';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/${id}`);
  }
  async getByIdCreatureVersion(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/versions/${id}`);
  }

  async createData(data: CreateCreatureVersion): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async updateData(data: UpdateCreatureVersion): Promise<ResponseWrapper<any>> {
    return await this.updateById(data.id, data);
  }

  async removeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.remove(id);
  }
}
