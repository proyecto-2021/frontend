import { ReportRestRepository, UpdateWizardReport } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';
import {
  CreateReport,
  UpdateReport,
  UpdateStatusReport,
  ClasificationStatusReport,
} from '@/types';

export class ReportRepository
  extends BaseRepository
  implements ReportRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/reports';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async createData(data: CreateReport): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.create(formData);
  }

  async updateDataById(data: UpdateReport): Promise<ResponseWrapper> {
    return await this.updateById(data.id, data);
  }

  async updateStatusData(data: UpdateStatusReport): Promise<ResponseWrapper> {
    return await this.patchResource(`status/${data.id}`, data);
  }

  async updateWizardData(data: UpdateWizardReport): Promise<ResponseWrapper> {
    return await this.patchResource(`wizarId/${data.id}`, data);
  }

  async clasificationData(
    data: ClasificationStatusReport
  ): Promise<ResponseWrapper<any>> {
    return await this.patchResource(`classificationReport/${data.id}`, data);
  }

  async removeData(id: string): Promise<ResponseWrapper> {
    return await this.remove(id);
  }

  async getByWizard(id: string): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/wizard/${id}`);
  }
}
