import { AdminRestRepository, CreateAdmin, UpdateAdmin } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class AdminRepository
  extends BaseRepository
  implements AdminRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/users';
  }
  async getAll(): Promise<ResponseWrapper<any>> {
    return await this.get();
  }
  async getUserType(type: string): Promise<ResponseWrapper<any>> {
    return await this.getResource(`type/${type}`);
  }
  async getByUsername(username: string): Promise<ResponseWrapper<any>> {
    return await this.getById(username);
  }
  async createData(data: CreateAdmin): Promise<ResponseWrapper<any>> {
    return await this.create(data);
  }
  async updateData(data: UpdateAdmin): Promise<ResponseWrapper<any>> {
    return await this.updateById(data.username, data);
  }
  async removeData(username: string): Promise<ResponseWrapper<any>> {
    return await this.remove(username);
  }
}
