import { MissionRestRepository, CreateMission, UpdateMission } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class MissionRepository
  extends BaseRepository
  implements MissionRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/misions';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/${id}`);
  }
  async getByIdCreatureVersion(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/version/${id}`);
  }

  async createData(data: CreateMission): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async updateData(data: UpdateMission): Promise<ResponseWrapper<any>> {
    return await this.updateById(data.id, data);
  }

  async removeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.remove(id);
  }

  async activeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.updateResource(`/activate/${id}`, {});
  }
}
