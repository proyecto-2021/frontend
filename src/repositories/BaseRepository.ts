import { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ResponseWrapper, ErrorWrapper } from '@/services/utils';
import store from '@/store';
import qs from 'qs';
export abstract class BaseRepository {
  protected $axios!: AxiosInstance;

  protected constructor($axios: AxiosInstance) {
    this.$axios = $axios;
    this.configureInterceptor();
  }

  get entity(): string {
    throw new Error('"entity" getter not defined');
  }

  private axiosConfig = {
    paramsSerializer: (p: any) => qs.stringify(p),
  };

  async get<T = any>(parameters = {}): Promise<ResponseWrapper<T>> {
    const config: AxiosRequestConfig = {
      params: { ...parameters },
      paramsSerializer: this.axiosConfig.paramsSerializer,
    };
    try {
      const response = await this.$axios.get<T>(this.entity, config);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async getResource<T = any>(
    resourceName: string,
    parameters = {}
  ): Promise<ResponseWrapper<T>> {
    const config: AxiosRequestConfig = {
      params: { ...parameters },
      paramsSerializer: this.axiosConfig.paramsSerializer,
    };
    try {
      const response = await this.$axios.get<T>(
        `${this.entity}/${resourceName}`,
        config
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async getExternalResource<T = any>(
    externalUrl: string,
    parameters = {}
  ): Promise<ResponseWrapper<T>> {
    const params = { ...parameters };
    try {
      const response = await this.$axios.get<T>(`${externalUrl}`, {
        ...this.axiosConfig,
        params,
      });
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async getById<T = any, G = any>(id: G): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.get<T>(`${this.entity}/${id}`);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async create<T = any>(data: T): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.post<T>(`${this.entity}`, data);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async createResource<T = any>(
    resourceName: string,
    data: T
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.post<T>(
        `${this.entity}/${resourceName}`,
        data,
        { ...this.axiosConfig }
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async update<T = any>(data: T): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.put<T>(`${this.entity}`, data);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async updateById<T = any, G = any>(
    id: G,
    data: T
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.put<T>(`${this.entity}/${id}`, data);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async updateResource<T = any>(
    resourceName: string,
    data: T
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.put<T>(
        `${this.entity}/${resourceName}`,
        data
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async remove<T = any, G = any>(id: G): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.delete<T>(`${this.entity}/${id}`);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async removeResource<T = any>(
    resourceName: string
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.delete<T>(
        `${this.entity}/${resourceName}`
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async patch<T = any>(data: T): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.patch<T>(`${this.entity}`, data);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async patchById<T = any, G = any>(
    id: G,
    data?: T
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.patch<T>(`${this.entity}/${id}`, data);
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async patchResource<T = any>(
    resourceName: string,
    data: T
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.patch<T>(
        `${this.entity}/${resourceName}`,
        data
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  async callAction<T = any>(
    resourceName: string,
    data?: T,
    config?: AxiosRequestConfig
  ): Promise<ResponseWrapper<T>> {
    try {
      const response = await this.$axios.put<T>(
        `${this.entity}/${resourceName}`,
        data,
        config
      );
      return new ResponseWrapper<T>(response);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  configureInterceptor() {
    this.$axios.interceptors.request.use(
      async (request: AxiosRequestConfig) => {
        request.baseURL = process.env.VUE_APP_API_URL ?? '';
        if (store.state.auth.isAuthenticated) {
          request.headers.authorization = `Bearer ${store.state.auth.currentAuthData.token}`;
        }
        // if (IdentityStore.isAuthenticated && await IdentityStore.isAccessTokenExpired() && IdentityStore.refreshToken
        //   && !request.headers.refreshToken) {
        //   try {
        //     await IdentityStore.refreshTokens();
        //     request.headers.authorization = `Bearer ${IdentityStore.accessToken}`;
        //     return Promise.resolve(request);
        //   }
        //   catch (error) {
        //     //window.$nuxt.$router.push({ name: "login" });
        //     return Promise.reject(error)
        //   }
        // }
        else {
          return request;
        }
        return Promise.resolve(request);
      },
      (error) => {
        return Promise.reject(error);
      }
    );
  }
}
