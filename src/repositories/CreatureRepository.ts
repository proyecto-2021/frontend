import {
  CreatureRestRepository,
  CreateCreature,
  UpdateCreature,
  UpdateAvatarCreature,
  CreatureResource,
} from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class CreatureRepository
  extends BaseRepository
  implements CreatureRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/creatures';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }
  async getByIdData(id: number): Promise<ResponseWrapper> {
    return await this.getById(id);
  }
  async getByUsername(username: string): Promise<ResponseWrapper> {
    return await this.getResource(`/user/${username}`);
  }
  async createData(data: CreateCreature): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.create(formData);
  }
  async updateData(data: UpdateCreature): Promise<ResponseWrapper> {
    const formData = new FormData();
    Object.entries(data).forEach(([key, value]) => {
      if (typeof value === 'object') {
        formData.append(`${key}`, value);
      } else {
        formData.append(`${key}`, `${value}`);
      }
    });
    return await this.updateById(data.id, formData);
  }

  async updateAvatar(data: UpdateAvatarCreature): Promise<ResponseWrapper> {
    return await this.updateResource(`avatar/${data.id}`, data);
  }
}
