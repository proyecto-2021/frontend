import { BlogRestRepository, Blog } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class BlogRepository
  extends BaseRepository
  implements BlogRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }
  get entity(): string {
    return '/api/v1/Blogs';
  }
  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async create(): Promise<ResponseWrapper> {
    return await this.create();
  }

  async update(): Promise<ResponseWrapper> {
    return await this.update();
  }
}
