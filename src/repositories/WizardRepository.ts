import { CreateWizard, UpdateWizard, WizardRestRepository } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class WizardRepository
  extends BaseRepository
  implements WizardRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/wizard';
  }

  async getAll(): Promise<ResponseWrapper> {
    return await this.get();
  }

  async getByIdData(id: string): Promise<ResponseWrapper<any>> {
    return await this.getById(id);
  }

  async getByUsername(username: string): Promise<ResponseWrapper> {
    return await this.getResource(`user/${username}`);
  }

  async createData(data: CreateWizard): Promise<ResponseWrapper> {
    return await this.create(data);
  }

  async UpdateDataById(data: UpdateWizard): Promise<ResponseWrapper> {
    return await this.updateById(data.id, data);
  }
}
