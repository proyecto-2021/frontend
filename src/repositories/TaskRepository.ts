import { TaskRestRepository, CreateTask, updateTask } from '@/types';
import { AxiosInstance } from 'axios';
import { ResponseWrapper } from '@/services/utils';
import { BaseRepository } from './BaseRepository';

export class TaskRepository
  extends BaseRepository
  implements TaskRestRepository {
  constructor($axios: AxiosInstance) {
    super($axios);
  }

  get entity(): string {
    return '/api/v1/task';
  }

  async getAll(): Promise<ResponseWrapper<any>> {
    return await this.get();
  }

  async getByIdData(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/${id}`);
  }

  async getByIdTask(id: number): Promise<ResponseWrapper<any>> {
    return await this.getResource(`/${id}`);
  }
  async createData(data: CreateTask): Promise<ResponseWrapper<any>> {
    return await this.create(data);
  }
  async updateData(data: updateTask): Promise<ResponseWrapper<any>> {
    return await this.updateById(data.id, data);
  }
  async removeData(id: number): Promise<ResponseWrapper<any>> {
    return await this.remove(id);
  }
}
