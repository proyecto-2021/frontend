import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

const theme = {
  primary: '#4847b5',
  primaryVariant: '#8aa39b',
  secondary: '#0f444c',
  secondaryVariant: '#b5e3c5',
  tertiary: '#ffebff',
  info: '#00CAE3',
  success: '#4CAF50',
  warning: '#FB8C00',
  error: '#FF5252',
  gray1: '#161616',
  gray2: '#EEEEEE',
  white: '#FFFFFF',
};
export default new Vuetify({
  theme: {
    themes: {
      dark: theme,
      light: theme,
    },
  },
});
