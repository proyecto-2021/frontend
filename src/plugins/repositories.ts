import { initializeRepositories } from '@/repositories';
import axios from 'axios';

initializeRepositories(axios);
