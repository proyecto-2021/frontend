import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import VueSweetalert2 from 'vue-sweetalert2';
import '@/plugins/repositories';
import 'sweetalert2/dist/sweetalert2.min.css';
import '@/plugins/apexcharts';
import VueMapbox from 'vue-mapbox';
import Mapbox from 'mapbox-gl';

Vue.use(VueMapbox, { mapboxgl: Mapbox });

Vue.use(VueSweetalert2);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
