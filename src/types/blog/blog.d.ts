export interface Blog {
  titulo: string;
  descripcion: string;
  autor: string;
}
