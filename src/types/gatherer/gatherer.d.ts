import { User } from '@/types';

export interface Gatherer {
  id: number;
  avatar: string;
  score: number;
  stars: number;
  user: User;
}

export interface GathererResource {
  id: number;
  avatar: string;
  score: number;
  stars: number;
  username: string;
}

export interface CreateGatherer {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  avatar?: Event;
  score: number;
  stars: number;
}

export interface UpdateGatherer {
  id: number;
  score: number;
  stars: number;
  avatar?: Event;
}
