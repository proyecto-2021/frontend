import { MissionResource } from '@/types';

export interface MissionTask {
  id: number;
  name: string;
  description: string;
  score: number;
  isActive: boolean;
  mission: MissionResource;
}

export interface MissionTaskResource {
  id: number;
  missionId: number;
  name: string;
  description: string;
  isActive: boolean;
}

export interface CreateMissionTask {
  missionId: number;
  name: string;
  description: string;
  score: number;
}

export interface UpdateMissionTask {
  id: number;
  name: string;
  description: string;
  score: number;
}
