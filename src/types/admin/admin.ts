import { UserType } from '@/enums';

export interface Admin {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  type: UserType;
  isActive: boolean;
  password: string;
}

export interface CreateAdmin {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface UpdateAdmin {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
}
