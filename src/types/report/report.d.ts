import {
  ReportType,
  ClassificationReportType,
  StatusReportType,
} from '@/enums';
import {
  MissionTaskResource,
  CreatureVersionResource,
  WizardResource,
  GathererResource,
  Geolocation,
} from '@/types';
export interface Report {
  id: string;
  date: string;
  assignedAt: string;
  endsAt: string;
  deliveryAt: string;
  solution: string;
  type: ReportType;
  description: string;
  image: string;
  classificationReport: ClassificationReportType;
  status: StatusReportType;
  collector: GathererResource;
  creatureVersion: CreatureVersionResource;
  subMission: MissionTaskResource;
  wizard: WizardResource;
  geolocation: Geolocation;
}

export interface ReportResource {
  id: string;
  date: string;
  assignedAt: string;
  endsAt: string;
  deliveryAt: string;
  solution: string;
  type: ReportType;
  description: string;
  image: string;
  classificationReport: ClassificationReportType;
  status: StatusReportType;
  collectorId: number;
  creatureVersionId: number;
  subMissionId: number;
  wizardId: number;
}

export interface CreateReport {
  type: ReportType;
  description: string;
  image?: Event;
  classificationReport: ClassificationReportType;
  status: StatusReportType;
  collectorId: number;
  creatureVersionId: number;
  subMissionId: number;
  wizardId?: string;
}

export interface UpdateReport {
  id: string;
  type: ReportType;
  description: string;
  image?: string;
  classificationReport: ClassificationReportType;
  status: StatusReportType;
  collectorId: number;
  creatureVersionId: number;
  subMissionId: number;
  wizardId?: string;
}

export interface UpdateStatusReport {
  id: string;
  status: string;
  solution?: string;
}

export interface UpdateWizardReport {
  id: string;
  wizardId: string;
  endsAt: string;
}
export interface ClasificationStatusReport {
  id: string;
  classificationReport: string;
}
