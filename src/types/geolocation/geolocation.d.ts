export interface Geolocation {
  country: string;
  countryName?: string;
  region: string;
  timezone: string;
  city: string;
  ll: Array<number>;
}
