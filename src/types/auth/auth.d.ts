import { UserType } from '@/enums';
export interface Login {
  username: string;
  password: string;
}

export interface RestartPassword {
  email: string;
}

export interface Register {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface ResponseAuth {
  name: string;
  token: string;
  username: string;
  type: UserType;
}
