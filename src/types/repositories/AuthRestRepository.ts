import { ResponseWrapper } from '@/services/utils';
import { Login, RestartPassword, Register, ResponseAuth } from '@/types';

export interface AuthRestRepository {
  login(data: Login): Promise<ResponseWrapper<ResponseAuth>>;
  restartPassword(
    data: RestartPassword
  ): Promise<ResponseWrapper<ResponseAuth>>;
  register(data: Register): Promise<ResponseWrapper>;
}
