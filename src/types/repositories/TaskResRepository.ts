//interfaces
import { ResponseWrapper } from '@/services/utils';
import { CreateTask, updateTask } from '@/types';

export interface TaskRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByIdTask(id: number): Promise<ResponseWrapper>;
  createData(data: CreateTask): Promise<ResponseWrapper>;
  updateData(data: updateTask): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
