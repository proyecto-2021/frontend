import { ResponseWrapper } from '@/services/utils';
import { CreateCreatureVersion, UpdateCreatureVersion } from '@/types';

export interface CreatureVersionRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByIdCreatureVersion(id: number): Promise<ResponseWrapper>;
  createData(data: CreateCreatureVersion): Promise<ResponseWrapper>;
  updateData(data: UpdateCreatureVersion): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
