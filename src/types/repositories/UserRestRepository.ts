import { ResponseWrapper } from '@/services/utils';
import { UpdatePassword, User, UpdatePasswords } from '@/types';

export interface UserRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByUsername(username: string): Promise<ResponseWrapper>;
  getUserProfile(): Promise<ResponseWrapper>;
  create(data: User): Promise<ResponseWrapper>;
  activeData(username: string): Promise<ResponseWrapper>;
  removeData(username: string): Promise<ResponseWrapper>;
  updatePassword(data: UpdatePassword): Promise<ResponseWrapper>;
  updatePasswords(data: UpdatePasswords): Promise<ResponseWrapper>;
}
