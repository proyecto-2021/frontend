import { ResponseWrapper } from '@/services/utils';
import { CreateAdmin, UpdateAdmin } from '@/types';

export interface AdminRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getUserType(type: string): Promise<ResponseWrapper>;
  getByUsername(username: string): Promise<ResponseWrapper>;
  createData(data: CreateAdmin): Promise<ResponseWrapper>;
  updateData(data: UpdateAdmin): Promise<ResponseWrapper>;
  removeData(username: string): Promise<ResponseWrapper>;
}
