import { ResponseWrapper } from '@/services/utils';
import {
  CreateReport,
  UpdateReport,
  UpdateStatusReport,
  UpdateWizardReport,
  ClasificationStatusReport,
} from '@/types';

export interface ReportRestRepository {
  getAll(): Promise<ResponseWrapper>;
  createData(data: CreateReport): Promise<ResponseWrapper>;
  updateDataById(data: UpdateReport): Promise<ResponseWrapper>;
  updateStatusData(data: UpdateStatusReport): Promise<ResponseWrapper>;
  clasificationData(data: ClasificationStatusReport): Promise<ResponseWrapper>;
  removeData(id: string): Promise<ResponseWrapper>;
  getByWizard(id: string): Promise<ResponseWrapper>;
  updateWizardData(date: UpdateWizardReport): Promise<ResponseWrapper>;
}
