import { ResponseWrapper } from '@/services/utils';

export interface BlogRestRepository {
  getAll(): Promise<ResponseWrapper>;
  create(): Promise<ResponseWrapper>;
  update(): Promise<ResponseWrapper>;
}
