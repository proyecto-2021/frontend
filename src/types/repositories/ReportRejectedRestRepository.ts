import { ResponseWrapper } from '@/services/utils';
import { CreateReportRejected, UpdateReportRejected } from '@/types';

export interface ReportRejectedRestRepository {
  getAll(): Promise<ResponseWrapper>;
  createData(data: CreateReportRejected): Promise<ResponseWrapper>;
  updateDataById(data: UpdateReportRejected): Promise<ResponseWrapper>;
  removeData(id: string): Promise<ResponseWrapper>;
}
