import { ResponseWrapper } from '@/services/utils';

export interface DashboardRestRepository {
  getData(): Promise<ResponseWrapper>;
}
