import { ResponseWrapper } from '@/services/utils';
import { CreateCreature, UpdateCreature, UpdateAvatarCreature } from '@/types';

export interface CreatureRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByUsername(username: string): Promise<ResponseWrapper>;
  createData(data: CreateCreature): Promise<ResponseWrapper>;
  updateData(data: UpdateCreature): Promise<ResponseWrapper>;
  updateAvatar(data: UpdateAvatarCreature): Promise<ResponseWrapper>;
}
