import { ResponseWrapper } from '@/services/utils';
import { CreateGatherer, UpdateGatherer } from '@/types';

export interface GathererRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByUsername(username: string): Promise<ResponseWrapper>;
  createData(data: CreateGatherer): Promise<ResponseWrapper>;
  updateDataById(data: UpdateGatherer): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
