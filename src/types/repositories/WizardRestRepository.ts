import { ResponseWrapper } from '@/services/utils';
import { CreateWizard, UpdateWizard } from '@/types';

export interface WizardRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: string): Promise<ResponseWrapper>;
  getByUsername(username: string): Promise<ResponseWrapper>;
  createData(data: CreateWizard): Promise<ResponseWrapper>;
  UpdateDataById(data: UpdateWizard): Promise<ResponseWrapper>;
}
