import { ResponseWrapper } from '@/services/utils';
import { CreateMission, UpdateMission } from '@/types';

export interface MissionRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByIdCreatureVersion(id: number): Promise<ResponseWrapper>;
  createData(data: CreateMission): Promise<ResponseWrapper>;
  updateData(data: UpdateMission): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
  activeData(id: number): Promise<ResponseWrapper>;
}
