import { ResponseWrapper } from '@/services/utils';
import { CreateMissionTask, UpdateMissionTask } from '@/types';

export interface MissionTaskRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  getByMissionId(id: number): Promise<ResponseWrapper>;
  createData(data: CreateMissionTask): Promise<ResponseWrapper>;
  updateData(data: UpdateMissionTask): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
