import { ResponseWrapper } from '@/services/utils';
import { CreateRole, UpdateRole } from '@/types';

export interface RoleRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getAllActives(): Promise<ResponseWrapper>;
  getByIdData(id: string): Promise<ResponseWrapper>;
  createData(data: CreateRole): Promise<ResponseWrapper>;
  updateData(data: UpdateRole): Promise<ResponseWrapper>;
  activeData(id: number): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
