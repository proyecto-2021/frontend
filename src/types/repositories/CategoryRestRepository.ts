import { ResponseWrapper } from '@/services/utils';
import { CreateCategory, UpdateCategory } from '@/types';

export interface CategoryRestRepository {
  getAll(): Promise<ResponseWrapper>;
  getByIdData(id: number): Promise<ResponseWrapper>;
  createData(data: CreateCategory): Promise<ResponseWrapper>;
  updateData(data: UpdateCategory): Promise<ResponseWrapper>;
  removeData(id: number): Promise<ResponseWrapper>;
}
