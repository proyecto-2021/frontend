export interface User {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  type: string;
  isActive: boolean;
}
export interface UpdatePassword {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

export interface UpdatePasswords {
  uername: string;
  password: number;
}
