import { CreatureResource } from '@/types';

export interface CreatureVersion {
  id: number;
  name: string;
  url: string;
  isActive: boolean;
  creature: Creature;
}

export interface CreatureVersionResource {
  id: number;
  creatureId: number;
  name: string;
  url: string;
  isActive: boolean;
}

export interface CreateCreatureVersion {
  creatureId: number;
  name: string;
  url: string;
  isActive: boolean;
}

export interface UpdateCreatureVersion {
  id: nunmber;
  name: string;
  url: string;
}
