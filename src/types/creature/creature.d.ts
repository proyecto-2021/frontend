import { User, CreatureVersionResource, Category } from '@/types';

export interface Creature {
  id: number;
  description: string;
  life: number;
  avatar: string;
  lat?: number;
  lgn?: number;
  lastVersion: CreatureVersionResource;
  category: Category;
  user: User;
}

export interface CreatureResource {
  id: number;
  description: string;
  life: number;
  avatar: string;
  username: string;
  lat?: number;
  lgn?: number;
  categoryId: number;
}

export interface CreateCreature {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  description: string;
  life: number;
  avatar?: Event;
  lat?: number;
  lgn?: number;
  categoryId: number;
}

export interface UpdateCreature {
  id: number;
  description: string;
  life: number;
  avatar?: Event;
  lat?: number;
  lgn?: number;
  categoryId: number;
}
export interface UpdateAvatarCreature {
  id: number;
  avatar: string;
}
