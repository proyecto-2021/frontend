import { CreatureVersionResource } from '@/types';

export interface Mission {
  id: number;
  creatureVersion: CreatureVersionResource;
  name: string;
  description: string;
  isActive: boolean;
}

export interface MissionResource {
  id: number;
  creatureVersionId: number;
  name: string;
  description: string;
  isActive: boolean;
}

export interface CreateMission {
  creatureVersionId?: number;
  name: string;
  description: string;
}

export interface UpdateMission {
  id: number;
  name: string;
  description: string;
}
