export interface Role {
  id: number;
  name: string;
  description: string;
  avatar: '';
  isActive: boolean;
}
export interface CreateRole {
  name: string;
  description: string;
  avatar?: Event;
}

export interface UpdateRole {
  id: number;
  name: string;
  description: string;
  avatar?: Event;
}

export interface Rol {
  id: number;
  name: string;
  wizardCount: number;
}
