import { Wizard, Report } from '@/types';
import { ReportResource } from '../report';
import { WizardResource } from '../wizard';

export interface Task {
  id: number;
  name: string;
  description: string;
  date: string;
  status: string;
  wizard: WizardResource;
  report: ReportResource;
}

export interface TaskResource {
  id: number;
  wizardId: number;
  reportId: number;
  name: string;
  description: string;
  date: Date;
  status: string;
}

export interface CreateTask {
  wizardId: number;
  reportId: number;
  name: string;
  description: string;
  date: any;
}

export interface updateTask {
  id: number;
  wizardId: number;
  reportId: number;
  name: string;
  description: string;
  date: any;
  status?: string;
}
