import { Rol } from '@/types';

export interface dashboard {
  creatures: number;
  delayeds: number;
  improvements: number;
  errors: number;
  wizards: number;
  roles: Rol;
}
