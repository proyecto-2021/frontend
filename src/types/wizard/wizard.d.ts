import { User, CreatureResource, Role } from '@/types';

export interface Wizard {
  id: string;
  score: number;
  user: User;
  role: Role;
  creature: CreatureResource;
}

export interface WizardResource {
  id: string;
  score: number;
  username: string;
  roleId: number;
  creatureId: number;
}

export interface CreateWizard {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  roleId: number;
  creatureId: number;
  score: number;
}
export interface UpdateWizard {
  id: string;
  roleId: number;
  creatureId: number;
  score: number;
}
