export interface ItemNav {
  icon: string;
  name: string;
  url: string;
}
