import { WizardResource, ReportResource } from '@/types';

export interface ReportRejected {
  id: string;
  description: string;
  report: ReportResource;
  wizard: WizardResource;
}

export interface CreateReportRejected {
  reportId: string;
  wizardId: string;
  description: string;
}

export interface UpdateReportRejected {
  id: string;
  description: string;
  reportId: string;
  wizardId: string;
}
