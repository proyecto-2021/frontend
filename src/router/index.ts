import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import store from '@/store';
import Layout from '@/layouts/default/Index.vue';
import Admin from '@/views/Admin.vue';
import Creature from '@/views/Creature.vue';
import Wizard from '@/views/Wizard.vue';
import Gatherer from '@/views/Gatherer.vue';
import Dashboard from '@/views/Dashboard.vue';
import Mission from '@/views/Mission.vue';
import MissionTask from '@/views/MissionTask.vue';
import Report from '@/views/Report.vue';
import RejectedReport from '@/views/RejectedReport.vue';
import Role from '@/views/Role.vue';
import Profile from '@/views/Profile.vue';
import Login from '@/views/Login.vue';
import Forgot from '@/views/Forgot.vue';
import CreatureVersion from '@/views/CreatureVersion.vue';
import Task from '@/views/Task.vue';
import BoardWizard from '@/views/BoardWizard.vue';
import Category from '@/views/Category.vue';
import { UserType } from '@/enums';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Login',
    meta: {
      requiresAuth: false,
    },
    component: Login,
  },
  {
    path: '/forgot',
    name: 'Forgot',
    meta: {
      requiresAuth: false,
    },
    component: Forgot,
  },
  {
    path: '/',
    name: 'layout',
    meta: {
      requiresAuth: true,
    },
    component: Layout,
    children: [
      {
        path: '/user/admin',
        name: 'Admin',
        meta: {
          requiresAuth: true,
        },
        component: Admin,
      },
      {
        path: '/user/creature',
        name: 'Creature',
        meta: {
          requiresAuth: true,
        },
        component: Creature,
      },
      {
        path: '/creature-version',
        name: 'CreatureVersion',
        meta: {
          requiresAuth: true,
        },
        component: CreatureVersion,
      },
      {
        path: '/task',
        name: 'Task',
        meta: {
          requiresAuth: true,
        },
        component: Task,
      },
      {
        path: '/user/wizard',
        name: 'Wizard',
        meta: {
          requiresAuth: true,
        },
        component: Wizard,
      },
      {
        path: '/user/gatherer',
        name: 'Gatherer',
        meta: {
          requiresAuth: true,
        },
        component: Gatherer,
      },
      {
        path: '/dashboard',
        name: 'Dashboard',
        meta: {
          requiresAuth: true,
        },
        component: Dashboard,
      },
      {
        path: '/mission',
        name: 'Mission',
        meta: {
          requiresAuth: true,
        },
        component: Mission,
      },
      {
        path: '/mission/:id/missionTask',
        name: 'MissionTask',
        meta: {
          requiresAuth: true,
        },
        component: MissionTask,
      },
      {
        path: '/report',
        name: 'Report',
        meta: {
          requiresAuth: true,
        },
        component: Report,
      },
      {
        path: '/rejectedReport',
        name: 'RejectedReport',
        meta: {
          requiresAuth: true,
        },
        component: RejectedReport,
      },
      {
        path: '/role',
        name: 'Role',
        meta: {
          requiresAuth: true,
        },
        component: Role,
      },
      {
        path: '/profile',
        name: 'Profile',
        meta: {
          requiresAuth: true,
        },
        component: Profile,
      },
      {
        path: '/boardwizard',
        name: 'BoardWizard',
        meta: {
          requiresAuth: true,
        },
        component: BoardWizard,
      },
      {
        path: '/category',
        name: 'Category',
        meta: {
          requiresAuth: true,
        },
        component: Category,
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.state.auth.isAuthenticated) {
      switch (store.state.auth.user.type) {
        case UserType.Creature:
          if (
            to.name === 'Wizard' ||
            to.name === 'Mission' ||
            to.name === 'MissionTask' ||
            to.name === 'CreatureVersion' ||
            to.name === 'Profile' ||
            to.name === 'Dashboard' ||
            to.name === 'BoardWizard' ||
            to.name === 'MissionTask'
          ) {
            next();
          } else {
            next({ name: 'Dashboard' });
          }
          break;
        case UserType.Wizard:
          if (
            to.name === 'Wizard' ||
            to.name === 'Mission' ||
            to.name === 'MissionTask' ||
            to.name === 'CreatureVersion' ||
            to.name === 'Profile' ||
            to.name === 'Dashboard' ||
            to.name === 'Report' ||
            to.name === 'RejectedReport' ||
            to.name === 'BoardWizard' ||
            to.name === 'Task' ||
            to.name === 'MissionTask'
          ) {
            next();
          } else {
            next({ name: 'Dashboard' });
          }
          break;
        case UserType.Administrator:
          if (
            to.name === 'Admin' ||
            to.name === 'Wizard' ||
            to.name === 'Creature' ||
            to.name === 'Gatherer' ||
            to.name === 'Mission' ||
            to.name === 'Profile' ||
            to.name === 'Dashboard' ||
            to.name === 'Role' ||
            to.name === 'Category'
          ) {
            next();
          } else {
            next({ name: 'Dashboard' });
          }
          break;
        default:
          break;
      }
      next();
    } else {
      next({ name: 'Login' });
    }
  } else {
    if (store.state.auth.isAuthenticated) {
      if (to.name === 'Login' || to.name === 'Forgot') {
        next({ name: 'Dashboard' });
      } else {
        next();
      }
    } else {
      next();
    }
  }
});

export default router;
