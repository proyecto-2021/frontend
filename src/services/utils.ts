import { AxiosResponse, AxiosError } from 'axios';

/**
 * Return message for HTTP status code
 * @param {number} status - HTTP status code
 * @returns {string} Message of network operation
 */
function _getStatusMessage(status: number): string {
  let message = '';
  switch (status) {
    case 200:
      message = 'All done. Request successfully executed';
      break;
    case 201:
      message = 'Data successfully created';
      break;
    case 204:
      message = 'No Content';
      break;
    case 400:
      message = 'Bad Request';
      break;
    case 401:
      message = 'Need auth';
      break;
    case 404:
      message = 'Not found';
      break;
    case 503:
      message = 'Service unavailable. Try again later';
      break;
    default:
      message = 'Something wrong. Client default error message';
      break;
  }
  return message;
}

function _getResponseErrorMessage(error: AxiosError): string {
  if (error.response && error.response.data) return error.response.data.message;
  if (error.response && error.response.statusText)
    return error.response.statusText;
  return error.message === 'Network Error'
    ? 'Oops! Network Error. Try again later'
    : error.message;
}
/**
 * El objeto de error
 * @param data
 * @returns
 */
function _getErrors(data: any): Array<ErrorEntry> {
  if (data?.errors && Array.isArray(data.errors)) {
    return data.errors.map((err: any) => {
      return { key: err.key, value: err.value } as ErrorEntry;
    });
  } else if (data && !Array.isArray(data)) {
    return Object.entries(data).map(([key, value]) => {
      return { key, value } as ErrorEntry;
    });
  }
  return [];
}

/**
 * Create instant, which represent response object
 * @param {Object} [data] - custom data
 * @param {Object} [response] - axios response object
 * @param {String} [message] - custom message to display
 */
export class ResponseWrapper<T = any> {
  data: T;
  success: boolean;
  status: number;
  statusMessage: string;
  message?: string;

  constructor(response: AxiosResponse<T>, message?: string) {
    this.data = response.data;
    this.success = !!response.data;
    this.status = response.status;
    this.statusMessage = _getStatusMessage(this.status);
    this.message = message || undefined;
  }
}

/**
 * Create instant, which represent error object
 * @param {Object} [error] - axios error object
 * @param {String} [message] - custom message to display
 */
export class ErrorWrapper extends Error {
  success: boolean;
  meta: boolean;
  code: boolean;
  status: number;
  statusMessage: string;
  errors: Array<ErrorEntry>;

  constructor(error: AxiosError) {
    super();
    const messageError =
      error.response?.data?.error ?? error.response?.statusText ?? '';
    this.errors = _getErrors(error.response?.data); // error.response?.data?.errors ?? [];
    this.success = error.response?.data?.success ?? false;
    this.meta = error.response?.data?.meta ?? false;
    this.code = error.response?.data?.code ?? false;
    this.status = error.response ? error.response.status : 500;
    this.statusMessage = _getStatusMessage(this.status);
    this.message = messageError || _getResponseErrorMessage(error);
  }
}
export interface ErrorEntry {
  key: string;
  value: any;
}

export interface Data<T> {
  data: T;
  value: string;
}
