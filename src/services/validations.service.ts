export default class ValidationService {
  static requiredField(value: string) {
    return !!value || 'Requerido';
  }

  static emailField(value: string) {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(value) || 'Correo electrónico invalido';
  }

  static passwordField(value: string) {
    const pattern = /(?=^.{8,}$)(?=.*?\d)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^a-zA-Z\d])/;
    return (
      pattern.test(value) ||
      'Contraseña debe tener 6 o más carácteres, poseer letras minusculas, mayusculas, números y caracteres especiales'
    );
  }
  static samePasswordField(newPassword: string, confirmPassword: string) {
    return newPassword === confirmPassword || 'Las contraseñas son distintas';
  }
}
