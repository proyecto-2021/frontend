export enum ClassificationReportType {
  None = '',
  Hight = 'H',
  Medium = 'M',
  Low = 'L',
}
