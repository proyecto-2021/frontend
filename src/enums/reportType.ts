export enum ReportType {
  None = '',
  GetBetter = 'I',
  Error = 'E',
}
