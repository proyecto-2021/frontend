import { Countries } from './countries';

export class CountryCode {
  public static countries: Array<CountryData> = [
    { country: Countries.None, countryId: 0, countryName: '' },
    { country: Countries.AF, countryId: 1, countryName: 'Afganistán' }, //Afganistán
    { country: Countries.AL, countryId: 2, countryName: 'Albania' }, //Albania
    { country: Countries.DE, countryId: 3, countryName: 'Alemania' }, //Alemania
    { country: Countries.AD, countryId: 4, countryName: 'Andorra' }, //Andorra
    { country: Countries.AO, countryId: 5, countryName: 'Angola' }, //Angola
    { country: Countries.AI, countryId: 6, countryName: 'Anguila' }, //Anguila
    { country: Countries.AQ, countryId: 7, countryName: 'Antártida' }, //Antártida
    { country: Countries.AG, countryId: 8, countryName: 'Barbuda' }, //Antigua y Barbuda
    { country: Countries.SA, countryId: 9, countryName: 'Saudí' }, //Arabia Saudí
    { country: Countries.DZ, countryId: 10, countryName: 'Argelia' }, //Argelia
    { country: Countries.AR, countryId: 11, countryName: 'Argentina' }, //Argentina
    { country: Countries.AM, countryId: 12, countryName: 'Armenia' }, //Armenia
    { country: Countries.AW, countryId: 13, countryName: 'Aruba' }, //Aruba
    { country: Countries.AU, countryId: 14, countryName: 'Australia' }, //Australia
    { country: Countries.AT, countryId: 15, countryName: 'Austria' }, //Austria
    { country: Countries.AZ, countryId: 16, countryName: 'Azerbaiyán' }, //Azerbaiyán
    { country: Countries.BS, countryId: 17, countryName: 'Bahamas' }, //Bahamas
    { country: Countries.BD, countryId: 18, countryName: 'Bangladés' }, //Bangladés
    { country: Countries.BB, countryId: 19, countryName: 'Barbados' }, //Barbados
    { country: Countries.BH, countryId: 20, countryName: 'Baréin' }, //Baréin
    { country: Countries.BE, countryId: 21, countryName: 'Bélgica' }, //Bélgica
    { country: Countries.BZ, countryId: 22, countryName: 'Belice' }, //Belice
    { country: Countries.BJ, countryId: 23, countryName: 'Benín' }, //Benín
    { country: Countries.BM, countryId: 24, countryName: 'Bermudas' }, //Bermudas
    { country: Countries.BY, countryId: 25, countryName: 'Bielorrusia' }, //Bielorrusia
    { country: Countries.BO, countryId: 26, countryName: 'Bolivia' }, //Bolivia
    { country: Countries.BA, countryId: 27, countryName: 'Herzegovina' }, //Bosnia-Herzegovina
    { country: Countries.BW, countryId: 28, countryName: 'Botsuana' }, //Botsuana
    { country: Countries.BR, countryId: 29, countryName: 'Brasil' }, //Brasil
    { country: Countries.BN, countryId: 30, countryName: 'Brunéi' }, //Brunéi
    { country: Countries.BG, countryId: 31, countryName: 'Bulgaria' }, //Bulgaria
    { country: Countries.BF, countryId: 32, countryName: 'Faso' }, //Burkina Faso
    { country: Countries.BI, countryId: 33, countryName: 'Burundi' }, //Burundi
    { country: Countries.BT, countryId: 34, countryName: 'Bután' }, //Bután
    { country: Countries.CV, countryId: 35, countryName: 'Verde' }, //Cabo Verde
    { country: Countries.KH, countryId: 36, countryName: 'Camboya' }, //Camboya
    { country: Countries.CM, countryId: 37, countryName: 'Camerún' }, //Camerún
    { country: Countries.CA, countryId: 38, countryName: 'Canadá' }, //Canadá
    { country: Countries.IC, countryId: 39, countryName: 'Canarias' }, //Canarias
    { country: Countries.BQ, countryId: 40, countryName: 'neerlandés' }, //Caribe neerlandés
    { country: Countries.QA, countryId: 41, countryName: 'Catar' }, //Catar
    { country: Countries.EA, countryId: 42, countryName: 'Melilla' }, //Ceuta y Melilla
    { country: Countries.TD, countryId: 43, countryName: 'Chad' }, //Chad
    { country: Countries.CZ, countryId: 44, countryName: 'Chequia' }, //Chequia
    { country: Countries.CL, countryId: 45, countryName: 'Chile' }, //Chile
    { country: Countries.CN, countryId: 46, countryName: 'China' }, //China
    { country: Countries.CY, countryId: 47, countryName: 'Chipre' }, //Chipre
    { country: Countries.VA, countryId: 48, countryName: 'Vaticano' }, //Ciudad del Vaticano
    { country: Countries.CO, countryId: 49, countryName: 'Colombia' }, //Colombia
    { country: Countries.KM, countryId: 50, countryName: 'Comoras' }, //Comoras
    { country: Countries.KP, countryId: 51, countryName: 'Norte' }, //Corea del Norte
    { country: Countries.KR, countryId: 52, countryName: 'Sur' }, //Corea del Sur
    { country: Countries.CR, countryId: 53, countryName: 'Rica' }, //Costa Rica
    { country: Countries.CI, countryId: 54, countryName: 'd’Ivoire' }, //Côte d’Ivoire
    { country: Countries.HR, countryId: 55, countryName: 'Croacia' }, //Croacia
    { country: Countries.CU, countryId: 56, countryName: 'Cuba' }, //Cuba
    { country: Countries.CW, countryId: 57, countryName: 'Curazao' }, //Curazao
    { country: Countries.DG, countryId: 58, countryName: 'García' }, //Diego García
    { country: Countries.DK, countryId: 59, countryName: 'Dinamarca' }, //Dinamarca
    { country: Countries.DM, countryId: 60, countryName: 'Dominica' }, //Dominica
    { country: Countries.EC, countryId: 61, countryName: 'Ecuador' }, //Ecuador
    { country: Countries.EG, countryId: 62, countryName: 'Egipto' }, //Egipto
    { country: Countries.SV, countryId: 63, countryName: 'Salvador' }, //El Salvador
    { country: Countries.AE, countryId: 64, countryName: 'Unidos' }, //Emiratos Árabes Unidos
    { country: Countries.ER, countryId: 65, countryName: 'Eritrea' }, //Eritrea
    { country: Countries.SK, countryId: 66, countryName: 'Eslovaquia' }, //Eslovaquia
    { country: Countries.SI, countryId: 67, countryName: 'Eslovenia' }, //Eslovenia
    { country: Countries.ES, countryId: 68, countryName: 'España' }, //España
    { country: Countries.US, countryId: 69, countryName: 'Unidos' }, //Estados Unidos
    { country: Countries.EE, countryId: 70, countryName: 'Estonia' }, //Estonia
    { country: Countries.ET, countryId: 71, countryName: 'Etiopía' }, //Etiopía
    { country: Countries.EZ, countryId: 72, countryName: 'Eurozone' }, //Eurozone
    { country: Countries.PH, countryId: 73, countryName: 'Filipinas' }, //Filipinas
    { country: Countries.FI, countryId: 74, countryName: 'Finlandia' }, //Finlandia
    { country: Countries.FJ, countryId: 75, countryName: 'Fiyi' }, //Fiyi
    { country: Countries.FR, countryId: 76, countryName: 'Francia' }, //Francia
    { country: Countries.GA, countryId: 77, countryName: 'Gabón' }, //Gabón
    { country: Countries.GM, countryId: 78, countryName: 'Gambia' }, //Gambia
    { country: Countries.GE, countryId: 79, countryName: 'Georgia' }, //Georgia
    { country: Countries.GH, countryId: 80, countryName: 'Ghana' }, //Ghana
    { country: Countries.GI, countryId: 81, countryName: 'Gibraltar' }, //Gibraltar
    { country: Countries.GD, countryId: 82, countryName: 'Granada' }, //Granada
    { country: Countries.GR, countryId: 83, countryName: 'Grecia' }, //Grecia
    { country: Countries.GL, countryId: 84, countryName: 'Groenlandia' }, //Groenlandia
    { country: Countries.GP, countryId: 85, countryName: 'Guadalupe' }, //Guadalupe
    { country: Countries.GU, countryId: 86, countryName: 'Guam' }, //Guam
    { country: Countries.GT, countryId: 87, countryName: 'Guatemala' }, //Guatemala
    { country: Countries.GF, countryId: 88, countryName: 'Francesa' }, //Guayana Francesa
    { country: Countries.GG, countryId: 89, countryName: 'Guernesey' }, //Guernesey
    { country: Countries.GN, countryId: 90, countryName: 'Guinea' }, //Guinea
    { country: Countries.GQ, countryId: 91, countryName: 'Ecuatorial' }, //Guinea Ecuatorial
    { country: Countries.GW, countryId: 92, countryName: 'Bisáu' }, //Guinea-Bisáu
    { country: Countries.GY, countryId: 93, countryName: 'Guyana' }, //Guyana
    { country: Countries.HT, countryId: 94, countryName: 'Haití' }, //Haití
    { country: Countries.HN, countryId: 95, countryName: 'Honduras' }, //Honduras
    { country: Countries.HU, countryId: 96, countryName: 'Hungría' }, //Hungría
    { country: Countries.IN, countryId: 97, countryName: 'India' }, //India
    { country: Countries.ID, countryId: 98, countryName: 'Indonesia' }, //Indonesia
    { country: Countries.IQ, countryId: 99, countryName: 'Irak' }, //Irak
    { country: Countries.IR, countryId: 100, countryName: 'Irán' }, //Irán
    { country: Countries.IE, countryId: 101, countryName: 'Irlanda' }, //Irlanda
    { country: Countries.AC, countryId: 102, countryName: 'Ascensión' }, //Isla de la Ascensión
    { country: Countries.IM, countryId: 103, countryName: 'Man' }, //Isla de Man
    { country: Countries.CX, countryId: 104, countryName: 'Navidad' }, //Isla de Navidad
    { country: Countries.NF, countryId: 105, countryName: 'Norfolk' }, //Isla Norfolk
    { country: Countries.IS, countryId: 106, countryName: 'Islandia' }, //Islandia
    { country: Countries.AX, countryId: 107, countryName: 'Åland' }, //Islas Åland
    { country: Countries.KY, countryId: 108, countryName: 'Caimán' }, //Islas Caimán
    { country: Countries.CC, countryId: 109, countryName: 'Cocos' }, //Islas Cocos
    { country: Countries.CK, countryId: 110, countryName: 'Cook' }, //Islas Cook
    { country: Countries.FO, countryId: 111, countryName: 'Feroe' }, //Islas Feroe
    { country: Countries.GS, countryId: 112, countryName: 'Sur' }, //Islas Georgia del Sur y Sandwich del Sur
    { country: Countries.FK, countryId: 113, countryName: 'Malvinas' }, //Islas Malvinas
    { country: Countries.MP, countryId: 114, countryName: 'Norte' }, //Islas Marianas del Norte
    { country: Countries.MH, countryId: 115, countryName: 'Marshall' }, //Islas Marshall
    { country: Countries.UM, countryId: 116, countryName: 'UU.' }, //Islas menores alejadas de EE. UU.
    { country: Countries.PN, countryId: 117, countryName: 'Pitcairn' }, //Islas Pitcairn
    { country: Countries.SB, countryId: 118, countryName: 'Salomón' }, //Islas Salomón
    { country: Countries.TC, countryId: 119, countryName: 'Caicos' }, //Islas Turcas y Caicos
    { country: Countries.VG, countryId: 120, countryName: 'Británicas' }, //Islas Vírgenes Británicas
    { country: Countries.VI, countryId: 121, countryName: 'UU.' }, //Islas Vírgenes de EE. UU.
    { country: Countries.IL, countryId: 122, countryName: 'Israel' }, //Israel
    { country: Countries.IT, countryId: 123, countryName: 'Italia' }, //Italia
    { country: Countries.JM, countryId: 124, countryName: 'Jamaica' }, //Jamaica
    { country: Countries.JP, countryId: 125, countryName: 'Japón' }, //Japón
    { country: Countries.JE, countryId: 126, countryName: 'Jersey' }, //Jersey
    { country: Countries.JO, countryId: 127, countryName: 'Jordania' }, //Jordania
    { country: Countries.KZ, countryId: 128, countryName: 'Kazajistán' }, //Kazajistán
    { country: Countries.KE, countryId: 129, countryName: 'Kenia' }, //Kenia
    { country: Countries.KG, countryId: 130, countryName: 'Kirguistán' }, //Kirguistán
    { country: Countries.KI, countryId: 131, countryName: 'Kiribati' }, //Kiribati
    { country: Countries.XK, countryId: 132, countryName: 'Kosovo' }, //Kosovo
    { country: Countries.KW, countryId: 133, countryName: 'Kuwait' }, //Kuwait
    { country: Countries.LA, countryId: 134, countryName: 'Laos' }, //Laos
    { country: Countries.LS, countryId: 135, countryName: 'Lesoto' }, //Lesoto
    { country: Countries.LV, countryId: 136, countryName: 'Letonia' }, //Letonia
    { country: Countries.LB, countryId: 137, countryName: 'Líbano' }, //Líbano
    { country: Countries.LR, countryId: 138, countryName: 'Liberia' }, //Liberia
    { country: Countries.LY, countryId: 139, countryName: 'Libia' }, //Libia
    { country: Countries.LI, countryId: 140, countryName: 'Liechtenstein' }, //Liechtenstein
    { country: Countries.LT, countryId: 141, countryName: 'Lituania' }, //Lituania
    { country: Countries.LU, countryId: 142, countryName: 'Luxemburgo' }, //Luxemburgo
    { country: Countries.MK, countryId: 143, countryName: 'Macedonia' }, //Macedonia
    { country: Countries.MG, countryId: 144, countryName: 'Madagascar' }, //Madagascar
    { country: Countries.MY, countryId: 145, countryName: 'Malasia' }, //Malasia
    { country: Countries.MW, countryId: 146, countryName: 'Malaui' }, //Malaui
    { country: Countries.MV, countryId: 147, countryName: 'Maldivas' }, //Maldivas
    { country: Countries.ML, countryId: 148, countryName: 'Mali' }, //Mali
    { country: Countries.MT, countryId: 149, countryName: 'Malta' }, //Malta
    { country: Countries.MA, countryId: 150, countryName: 'Marruecos' }, //Marruecos
    { country: Countries.MQ, countryId: 151, countryName: 'Martinica' }, //Martinica
    { country: Countries.MU, countryId: 152, countryName: 'Mauricio' }, //Mauricio
    { country: Countries.MR, countryId: 153, countryName: 'Mauritania' }, //Mauritania
    { country: Countries.YT, countryId: 154, countryName: 'Mayotte' }, //Mayotte
    { country: Countries.MX, countryId: 155, countryName: 'México' }, //México
    { country: Countries.FM, countryId: 156, countryName: 'Micronesia' }, //Micronesia
    { country: Countries.MD, countryId: 157, countryName: 'Moldavia' }, //Moldavia
    { country: Countries.MC, countryId: 158, countryName: 'Mónaco' }, //Mónaco
    { country: Countries.MN, countryId: 159, countryName: 'Mongolia' }, //Mongolia
    { country: Countries.ME, countryId: 160, countryName: 'Montenegro' }, //Montenegro
    { country: Countries.MS, countryId: 161, countryName: 'Montserrat' }, //Montserrat
    { country: Countries.MZ, countryId: 162, countryName: 'Mozambique' }, //Mozambique
    { country: Countries.MM, countryId: 163, countryName: 'Birmania)' }, //Myanmar (Birmania)
    { country: Countries.NA, countryId: 164, countryName: 'Namibia' }, //Namibia
    { country: Countries.NR, countryId: 165, countryName: 'Nauru' }, //Nauru
    { country: Countries.NP, countryId: 166, countryName: 'Nepal' }, //Nepal
    { country: Countries.NI, countryId: 167, countryName: 'Nicaragua' }, //Nicaragua
    { country: Countries.NE, countryId: 168, countryName: 'Níger' }, //Níger
    { country: Countries.NG, countryId: 169, countryName: 'Nigeria' }, //Nigeria
    { country: Countries.NU, countryId: 170, countryName: 'Niue' }, //Niue
    { country: Countries.NO, countryId: 171, countryName: 'Noruega' }, //Noruega
    { country: Countries.NC, countryId: 172, countryName: 'Caledonia' }, //Nueva Caledonia
    { country: Countries.NZ, countryId: 173, countryName: 'Zelanda' }, //Nueva Zelanda
    { country: Countries.OM, countryId: 174, countryName: 'Omán' }, //Omán
    { country: Countries.NL, countryId: 175, countryName: 'Bajos' }, //Países Bajos
    { country: Countries.PK, countryId: 176, countryName: 'Pakistán' }, //Pakistán
    { country: Countries.PW, countryId: 177, countryName: 'Palaos' }, //Palaos
    { country: Countries.PA, countryId: 178, countryName: 'Panamá' }, //Panamá
    { country: Countries.PG, countryId: 179, countryName: 'Guinea' }, //Papúa Nueva Guinea
    { country: Countries.PY, countryId: 180, countryName: 'Paraguay' }, //Paraguay
    { country: Countries.PE, countryId: 181, countryName: 'Perú' }, //Perú
    { country: Countries.PF, countryId: 182, countryName: 'Francesa' }, //Polinesia Francesa
    { country: Countries.PL, countryId: 183, countryName: 'Polonia' }, //Polonia
    { country: Countries.PT, countryId: 184, countryName: 'Portugal' }, //Portugal
    { country: Countries.PR, countryId: 185, countryName: 'Rico' }, //Puerto Rico
    { country: Countries.HK, countryId: 186, countryName: 'China)' }, //RAE de Hong Kong (China)
    { country: Countries.MO, countryId: 187, countryName: 'China)' }, //RAE de Macao (China)
    { country: Countries.GB, countryId: 188, countryName: 'Unido' }, //Reino Unido
    { country: Countries.CF, countryId: 189, countryName: 'Centroafricana' }, //República Centroafricana
    { country: Countries.CG, countryId: 190, countryName: 'Congo' }, //República del Congo
    { country: Countries.CD, countryId: 191, countryName: 'Congo' }, //República Democrática del Congo
    { country: Countries.DO, countryId: 192, countryName: 'Dominicana' }, //República Dominicana
    { country: Countries.RE, countryId: 193, countryName: 'Reunión' }, //Reunión
    { country: Countries.RW, countryId: 194, countryName: 'Ruanda' }, //Ruanda
    { country: Countries.RO, countryId: 195, countryName: 'Rumanía' }, //Rumanía
    { country: Countries.RU, countryId: 196, countryName: 'Rusia' }, //Rusia
    { country: Countries.EH, countryId: 197, countryName: 'Occidental' }, //Sáhara Occidental
    { country: Countries.WS, countryId: 198, countryName: 'Samoa' }, //Samoa
    { country: Countries.AS, countryId: 199, countryName: 'Americana' }, //Samoa Americana
    { country: Countries.BL, countryId: 200, countryName: 'Bartolomé' }, //San Bartolomé
    { country: Countries.KN, countryId: 201, countryName: 'Nieves' }, //San Cristóbal y Nieves
    { country: Countries.SM, countryId: 202, countryName: 'Marino' }, //San Marino
    { country: Countries.MF, countryId: 203, countryName: 'Martín' }, //San Martín
    { country: Countries.PM, countryId: 204, countryName: 'Miquelón' }, //San Pedro y Miquelón
    { country: Countries.VC, countryId: 205, countryName: 'Granadinas' }, //San Vicente y las Granadinas
    { country: Countries.SH, countryId: 206, countryName: 'Elena' }, //Santa Elena
    { country: Countries.LC, countryId: 207, countryName: 'Lucía' }, //Santa Lucía
    { country: Countries.ST, countryId: 208, countryName: 'Príncipe' }, //Santo Tomé y Príncipe
    { country: Countries.SN, countryId: 209, countryName: 'Senegal' }, //Senegal
    { country: Countries.RS, countryId: 210, countryName: 'Serbia' }, //Serbia
    { country: Countries.SC, countryId: 211, countryName: 'Seychelles' }, //Seychelles
    { country: Countries.SL, countryId: 212, countryName: 'Leona' }, //Sierra Leona
    { country: Countries.SG, countryId: 213, countryName: 'Singapur' }, //Singapur
    { country: Countries.SX, countryId: 214, countryName: 'Maarten' }, //Sint Maarten
    { country: Countries.SY, countryId: 215, countryName: 'Siria' }, //Siria
    { country: Countries.SO, countryId: 216, countryName: 'Somalia' }, //Somalia
    { country: Countries.LK, countryId: 217, countryName: 'Lanka' }, //Sri Lanka
    { country: Countries.SZ, countryId: 218, countryName: 'Suazilandia' }, //Suazilandia
    { country: Countries.ZA, countryId: 219, countryName: 'Sudáfrica' }, //Sudáfrica
    { country: Countries.SD, countryId: 220, countryName: 'Sudán' }, //Sudán
    { country: Countries.SS, countryId: 221, countryName: 'Sur' }, //Sudán del Sur
    { country: Countries.SE, countryId: 222, countryName: 'Suecia' }, //Suecia
    { country: Countries.CH, countryId: 223, countryName: 'Suiza' }, //Suiza
    { country: Countries.SR, countryId: 224, countryName: 'Surinam' }, //Surinam
    { country: Countries.SJ, countryId: 225, countryName: 'Mayen' }, //Svalbard y Jan Mayen
    { country: Countries.TH, countryId: 226, countryName: 'Tailandia' }, //Tailandia
    { country: Countries.TW, countryId: 227, countryName: 'Taiwán' }, //Taiwán
    { country: Countries.TZ, countryId: 228, countryName: 'Tanzania' }, //Tanzania
    { country: Countries.TJ, countryId: 229, countryName: 'Tayikistán' }, //Tayikistán
    { country: Countries.IO, countryId: 230, countryName: 'Índico' }, //Territorio Británico del Océano Índico
    { country: Countries.TF, countryId: 231, countryName: 'Franceses' }, //Territorios Australes Franceses
    { country: Countries.PS, countryId: 232, countryName: 'Palestinos' }, //Territorios Palestinos
    { country: Countries.TL, countryId: 233, countryName: 'Leste' }, //Timor-Leste
    { country: Countries.TG, countryId: 234, countryName: 'Togo' }, //Togo
    { country: Countries.TK, countryId: 235, countryName: 'Tokelau' }, //Tokelau
    { country: Countries.TO, countryId: 236, countryName: 'Tonga' }, //Tonga
    { country: Countries.TT, countryId: 237, countryName: 'Tobago' }, //Trinidad y Tobago
    { country: Countries.TA, countryId: 238, countryName: 'Acuña' }, //Tristán de Acuña
    { country: Countries.TN, countryId: 239, countryName: 'Túnez' }, //Túnez
    { country: Countries.TM, countryId: 240, countryName: 'Turkmenistán' }, //Turkmenistán
    { country: Countries.TR, countryId: 241, countryName: 'Turquía' }, //Turquía
    { country: Countries.TV, countryId: 242, countryName: 'Tuvalu' }, //Tuvalu
    { country: Countries.UA, countryId: 243, countryName: 'Ucrania' }, //Ucrania
    { country: Countries.UG, countryId: 244, countryName: 'Uganda' }, //Uganda
    { country: Countries.UN, countryId: 245, countryName: 'Nations' }, //United Nations
    { country: Countries.UY, countryId: 246, countryName: 'Uruguay' }, //Uruguay
    { country: Countries.UZ, countryId: 247, countryName: 'Uzbekistán' }, //Uzbekistán
    { country: Countries.VU, countryId: 248, countryName: 'Vanuatu' }, //Vanuatu
    { country: Countries.VE, countryId: 249, countryName: 'Venezuela' }, //Venezuela
    { country: Countries.VN, countryId: 250, countryName: 'Vietnam' }, //Vietnam
    { country: Countries.WF, countryId: 251, countryName: 'Futuna' }, //Wallis y Futuna
    { country: Countries.YE, countryId: 252, countryName: 'Yemen' }, //Yemen
    { country: Countries.DJ, countryId: 253, countryName: 'Yibuti' }, //Yibuti
    { country: Countries.ZM, countryId: 254, countryName: 'Zambia' }, //Zambia
    { country: Countries.ZW, countryId: 255, countryName: 'Zimbabue' }, //Zimbabue
  ];

  public static find(id: number): CountryData | undefined {
    return this.countries.find((a) => a.countryId === id);
  }
  public static findByCountry(country: Countries): CountryData {
    return this.countries.find((a) => a.country === country)!;
  }
}

export interface CountryData {
  country: Countries;
  countryId: number;
  countryName: string;
}
