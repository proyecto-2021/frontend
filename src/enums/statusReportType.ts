export enum StatusReportType {
  Pending = 'P',
  Process = 'PR',
  Tests = 'PB',
  Processed = 'PO',
  Rejected = 'R',
}
