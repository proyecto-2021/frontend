export enum UserType {
  Administrator = 'A',
  Wizard = 'W',
  Creature = 'C',
  Gatherer = 'CO',
}
