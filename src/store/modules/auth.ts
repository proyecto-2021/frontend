import {
  AuthRepository,
  UserRepository,
  WizardRepository,
  CreatureRepository,
} from '@/repositories';
import { ResponseWrapper, ErrorWrapper, ErrorEntry } from '@/services';
import { User, Login, ResponseAuth, Wizard, Creature } from '@/types';
import { UserType } from '@/enums';
export const namespaced = true;

export const state = {
  currentAuthData: {} as ResponseAuth,
  isAuthenticated: false,
  user: {} as User,
  wizard: {} as Wizard,
  creature: {} as Creature,
  creatureId: 0,
  isAdministrator: false,
  errorData: {} as ErrorWrapper,
};
export const mutations = {
  SET_SESSION(state: any, data: ResponseAuth): void {
    state.currentAuthData.name = data.name;
    state.currentAuthData.token = data.token;
    state.currentAuthData.username = data.username;
    state.currentAuthData.type = data.type;
    state.isAuthenticated = true;
    if (state.currentAuthData.type === UserType.Administrator) {
      state.isAdministrator = true;
    } else {
      state.isAdministrator = false;
    }
  },
  SET_WIZARD(state: any, data: Wizard): void {
    state.wizard = data;
    state.creatureId = data.creature.id;
  },
  SET_CREATURE(state: any, data: Creature): void {
    state.creature = data;
    state.creatureId = data.id;
  },
  CLOSE_SESSION(state: any): void {
    state.currentAuthData.name = '';
    state.currentAuthData.token = '';
    state.currentAuthData.username = '';
    state.currentAuthData.type = '';
    state.isAuthenticated = false;
    state.user = {};
    state.wizard = {};
    state.creature = {};
    state.creatureId = 0;
  },
  SET_USER(state: any, data: ResponseAuth): void {
    state.user = data;
  },
  SET_ERROR(state: any, data: ErrorWrapper): void {
    state.errorData = data;
  },
};

export const actions = {
  async login({ commit, dispatch }: any, data: Login): Promise<void> {
    try {
      const res: ResponseWrapper = await AuthRepository.login(data);
      if (res.data.data.type !== UserType.Gatherer) {
        commit('SET_SESSION', res.data.data);
        dispatch('currentUser');
        if (res.data.data.type === UserType.Wizard) {
          dispatch('currentWizard', res.data.data.username);
        }
        if (res.data.data.type === UserType.Creature) {
          dispatch('currentCreature', res.data.data.username);
        }
      } else {
        const err: ErrorWrapper = <ErrorWrapper>{
          success: false,
          meta: false,
          code: false,
          status: 401,
          statusMessage: '',
          errors: [
            {
              key: 'message',
              value: 'Usuario no válido para este entorno',
            } as ErrorEntry,
          ],
        };
        commit('SET_ERROR', err);
      }
    } catch (error) {
      if (error instanceof ErrorWrapper) {
        const err: ErrorWrapper = error as ErrorWrapper;
        commit('SET_ERROR', err);
      } else {
        console.log(error);
      }
    }
  },

  async currentWizard({ commit }: any, username: string): Promise<void> {
    try {
      const res: ResponseWrapper = await WizardRepository.getByUsername(
        username
      );
      commit('SET_WIZARD', res.data.data);
    } catch (error) {
      if (error instanceof ErrorWrapper) {
        const err: ErrorWrapper = error as ErrorWrapper;
        commit('SET_ERROR', err);
      } else {
        console.log(error);
      }
    }
  },

  async currentCreature({ commit }: any, username: string): Promise<void> {
    try {
      const res: ResponseWrapper = await CreatureRepository.getByUsername(
        username
      );
      commit('SET_CREATURE', res.data.data);
    } catch (error) {
      if (error instanceof ErrorWrapper) {
        const err: ErrorWrapper = error as ErrorWrapper;
        commit('SET_ERROR', err);
      } else {
        console.log(error);
      }
    }
  },

  async logout({ commit }: any): Promise<void> {
    commit('CLOSE_SESSION');
  },

  async currentUser({ commit }: any): Promise<void> {
    try {
      const res: ResponseWrapper = await UserRepository.getUserProfile();
      commit('SET_USER', res.data.data);
    } catch (error) {
      console.log(error);
    }
  },
};
export const getters = {};
