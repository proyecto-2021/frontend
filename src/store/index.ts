import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import * as auth from './modules/auth';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: true,
    mini: false,
  },
  getters: {},
  mutations: {
    UPDATE_DRAWER(state: any, drawer: boolean): void {
      state.drawer = drawer;
    },
    UPDATE_MINI(state: any): void {
      state.mini = !state.mini;
    },
  },
  actions: {
    updateDrawer({ commit }: any, drawer: boolean): void {
      commit('UPDATE_DRAWER', drawer);
    },
    updateMini({ commit }: any): void {
      commit('UPDATE_MINI');
    },
  },
  modules: {
    auth,
  },
  plugins: [createPersistedState()],
});
